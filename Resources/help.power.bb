[b]Power[/b]

There are two measurements to keep track of - power supply, and demand. If demand is larger than supply, the 
entire power grid will shut down. If supply is larger than demand, the excess can be used for [i]productivity[/i].

[b]Ways to reduce power consumption[/b]

The simplest way to reduce power consumption is to disable buildings in town. Disabling a building affects [i]Happiness[/i]. 

Building power demands are as follows:

[table=2]
[cell]Residence[/cell]
[cell]1[/cell]

[cell]Office building[/cell]
[cell]6[/cell]

[cell]Mall[/cell]
[cell]12[/cell]
[/table]
