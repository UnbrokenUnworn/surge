[b]Productivity[/b]

Productivity is generated when power supply exceeds demand. Each unit of excess power supply provides one productivity unit per minute.

For example, if something costs one productivity unit, it will take one minute to complete if power supply exceeds demand by 1, or 30 seconds if supply exceeds demand by 2.

[b]Productivity Projects[/b]

Projects:

[indent]Awareness Campaign: Residences in the active radius of the campaign will use less power while the campaign is active.[/indent]
[indent]...[/indent]
