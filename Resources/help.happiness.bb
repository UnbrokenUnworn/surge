[b]Happiness[/b]

Happiness is an important measure in the game, as a low happiness will come with productivity penalties.
Disabling power to a building will reduce the building's happiness output to 0. Happiness is measured as a percentage of produced happiness on potential happiness.

Building potential happiness yields are as follows:

[table=3]
[cell]Residence[/cell]
[cell]5[/cell]
[cell]One for each resident, where 2 are generated from proximity to jobs, and 3 from proximity to schools[/cell]

[cell]Office building[/cell]
[cell]12[/cell]
[cell]One for each employee, where 2 are generated for reach residence within its operating radius[/cell]

[cell]Mall[/cell]
[cell]16[/cell]
[cell]One for each residence within its operating radius[/cell]

[cell]Park[/cell]
[cell]10[/cell]
[cell]Parks add 10 happiness that cannot be reduced, as parks do not require power to operate[/cell]

[/table]

