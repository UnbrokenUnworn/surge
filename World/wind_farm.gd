extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$wind_turbine/AnimationPlayer.play("RotatePropellers")
	$wind_turbine2/AnimationPlayer.play("RotatePropellers")
	$wind_turbine3/AnimationPlayer.play("RotatePropellers")
	$wind_turbine4/AnimationPlayer.play("RotatePropellers")
	$wind_turbine5/AnimationPlayer.play("RotatePropellers")
	$wind_turbine6/AnimationPlayer.play("RotatePropellers")
	$wind_turbine7/AnimationPlayer.play("RotatePropellers")
	$wind_turbine8/AnimationPlayer.play("RotatePropellers")
	$wind_turbine9/AnimationPlayer.play("RotatePropellers")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
