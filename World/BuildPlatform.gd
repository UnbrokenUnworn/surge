extends Spatial
var rng = RandomNumberGenerator.new()
var current = "vacant"
var powered = false
var happiness = 0

var x = 0
var y = 0

var happiness_potential = {
	"neighborhood": 5,
	"solar_neighborhood": 5,
	"park": 10,
	"mall": 16,
	"offices": 12
}

var power_potential = {
	"powerplant": 50,
	"solar_farm": 15,
	"wind_farm": 25
}

var power_requirement = {
	"neighborhood": 8,
	"solar_neighborhood": 4,
	"mall": 32,
	"offices": 24
}
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func build(building):
	current = building
	$AnimationPlayer.play(building)

func power_supply():
	var amount = 0
	if current in power_potential:
		amount = power_potential[current]
	return amount

func power_demand():
	var amount = 0
	if current in power_requirement:
		amount = power_requirement[current]
	return amount

func recalculate_happiness():
	var nbrs = get_parent().neighbors(x, y)
	# print(nbrs)
	var happy = 0
	if "neighborhood" in current:
		for nbr in nbrs:
			if nbr == "offices":
				happy += 2
			if nbr == "mall":
				happy + 3
	if current == "offices":
		for nbr in nbrs:
			if "neighborhood" in nbr:
				happy += 2
	if current == "mall":
		for nbr in nbrs:
			if "neighborhood" in nbr:
				happy += 3
	if current == "park":
		happy = happiness_potential["park"]
	if current in happiness_potential:
		happiness = min(happy, happiness_potential[current])
	else:
		happiness = 0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	recalculate_happiness()
	$notifications/unhappy.hide()
	$notifications/unpowered.hide()
	if !powered and current in power_requirement:
		$notifications/unpowered.show()
	if happiness == 0 and current in happiness_potential:
		$notifications/unhappy.show()
# test
#func _input(event):
#	if event.is_action("ui_select") && event.is_pressed():
#		rng.randomize()
#		var options = [ "wind_farm", "solar_farm", "neighborhood", "solar_neighborhood", "park", "powerplant", "offices", "mall" ]
#		build(options[rng.randi_range(0, options.size() -1)])
#	if event.is_action("ui_focus_next") && event.is_pressed():
#		recalculate_happiness()
