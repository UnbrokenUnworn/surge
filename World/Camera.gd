extends Camera

# Called when the node enters the scene tree for the first time.
func _ready():
	translation.x = 24.945221
	translation.z = 32.664299

func _process(delta):
	if Input.is_action_pressed("ui_right"):
		translation.x += 1
	if Input.is_action_pressed("ui_left"):
		translation.x -= 1
	if Input.is_action_pressed("ui_up"):
		translation.z -= 1
	if Input.is_action_pressed("ui_down"):
		translation.z += 1

