extends Spatial

var map_width = 10
var map_height = 10

var power_supply = 100
var power_demand = 100
var productivity = 0
var happiness = 100

var map = []

onready var neighborhood = load("World/neighborhood.tscn")
onready var solar_neighborhood = load("World/solar_neighborhood.tscn")
onready var offices = load("World/offices.tscn")
onready var mall = load("World/mall.tscn")
onready var powerplant = load("World/powerplant.tscn")
onready var park = load("World/park.tscn")
onready var build_platform = load("World/BuildPlatform.tscn")

func create_town():
	var top_left = 4 
	var items = [
		"neighborhood",
		"neighborhood",
		"neighborhood",
		"neighborhood",
		"neighborhood",
		"neighborhood",
		"powerplant",
		"mall",
		"offices"
	]
	randomize()
	items.shuffle()
	for x in range(top_left, top_left + 3):
		for y in range(top_left, top_left + 3):
			map[x][y].build(items.pop_front())

func create_neighborhood(x, y):
	var nbh = build_platform.instance()
	nbh.translation.y = 0.5
	nbh.translation.x = x * 5
	nbh.translation.z = y * 5
	# game-grid coordinates
	nbh.x = x
	nbh.y = y
	add_child(nbh)
	return nbh

func neighbors(x, y):
	var nbrs = []
	for h in range(x - 1, x + 2):
		for v in range(y - 1, y + 2):
			if h >= 0 && h < map.size() && v >=0 && v < map[h].size():
				nbrs.append(map[h][v].current)
	return nbrs

func total_happiness():
	var happy = 0
	for x in range(0, map_width):
		for y in range(0, map_height):
			happy += map[x][y].happiness
	happiness = happy
	$CanvasLayer/current_stats/stats/happiness_stat/happiness.text = str(happy)
	

func total_power_supply():
	var supply = 0
	for x in range(0, map_width):
		for y in range(0, map_height):
			supply += map[x][y].power_supply()
	power_supply = supply
	$CanvasLayer/current_stats/stats/power_supply_stat/power_supply.text = str(supply)
	
func total_power_demand():
	var demand = 0
	for x in range(0, map_width):
		for y in range(0, map_height):
			demand += map[x][y].power_demand()
	power_demand = demand
	$CanvasLayer/current_stats/stats/power_demand_stat/power_demand.text = str(demand)

func _ready():
	map.resize(map_width)
	for x in range(0, map_width):
		map[x] = []
		map[x].resize(map_height)
		for y in range(0, map_height):
			map[x][y] = create_neighborhood(x, y)
	create_town()

func _process(delta):
	total_power_supply()
	total_power_demand()
	total_happiness()
	$CanvasLayer/current_stats/stats/productivity_stat/productivity.text = str(max(0, power_supply - power_demand))

