extends VBoxContainer

export(AudioStreamOGGVorbis) var hover_sound
export(AudioStreamOGGVorbis) var click_sound

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_Quit_pressed():
	GlobalAudio.stream = click_sound
	GlobalAudio.play()
	get_tree().quit()
	
func _on_Resume_pressed():
	GlobalAudio.stream = click_sound
	GlobalAudio.play()
	get_tree().change_scene("res://Landing/Credits.tscn")

func _on_New_Game_pressed():
	GlobalAudio.stream = click_sound
	GlobalAudio.play()
	get_tree().change_scene("res://World/World.tscn")

func _on_New_Game_mouse_entered():
	GlobalAudio.stream = hover_sound
	GlobalAudio.play()

func _on_Resume_mouse_entered():
	GlobalAudio.stream = hover_sound
	GlobalAudio.play()

func _on_Quit_mouse_entered():
	GlobalAudio.stream = hover_sound
	GlobalAudio.play()


